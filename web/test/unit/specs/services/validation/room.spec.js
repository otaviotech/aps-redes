import RoomValidator from '@/services/validation/room';

describe('RoomValidator', () => {
  let roomWithoutName;
  let roomWithNoUsers;
  let validRoom;

  beforeAll(() => {
    roomWithoutName = { name: '', maxUsers: '' };
    roomWithNoUsers = { name: '', maxUsers: 0 };
    validRoom = { name: 'Sala 1', maxUsers: 10 };
  });

  it('Deve identificar uma sala sem nome.', () => {
    const result = RoomValidator.validate(roomWithoutName);
    expect(result.success).toBeFalsy();
  });

  it('Deve identificar uma sala com um número de usuários inválidos.', () => {
    const result = RoomValidator.validate(roomWithNoUsers);
    expect(result.success).toBeFalsy();
  });

  it('Deve identificar uma sala válida.', () => {
    const result = RoomValidator.validate(validRoom);
    expect(result.success).toBeTruthy();
  });
});
