import Vue from 'vue';
import Router from 'vue-router';
import Logs from '@/components/Logs';
import Lobby from '@/components/Lobby';
import SignUp from '@/components/SignUp';
import Room from '@/components/room/Room';
import CreateRoom from '@/components/room/CreateRoom';
import ChatService from '@/services/chat';

Vue.use(Router);

const assertLoggedIn = (to, from, next) => {
  const user = ChatService.getUser();
  if (!user) {
    next({ name: 'SignUp' });
    return;
  }

  next();
};

const assertNotLoggedIn = (to, from, next) => {
  const user = ChatService.getUser();
  if (user) {
    next({ name: 'Lobby' });
    return;
  }

  next();
};

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SignUp',
      component: SignUp,
      beforeEnter: assertNotLoggedIn,
    },
    {
      path: '/lobby',
      name: 'Lobby',
      component: Lobby,
      beforeEnter: assertLoggedIn,
    },
    {
      path: '/logs',
      name: 'Logs',
      component: Logs,
    },
    {
      path: '/sala/:id',
      name: 'Room',
      component: Room,
      beforeEnter: assertLoggedIn,
    },
    {
      path: '/criar-sala',
      name: 'CreateRoom',
      component: CreateRoom,
      beforeEnter: assertLoggedIn,
    },
  ],
});
