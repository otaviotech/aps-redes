import Vue from 'vue';
import VeeValidate from 'vee-validate';
import moment from 'moment';
import VueToastr from '@deveodk/vue-toastr';
import App from './App';
import router from './router';
import vueToastrConfig from './config/vueToastrConfig';
import roomMessageFormatFilter from './filters/roomMessageFormat';

Vue.use(VeeValidate);
moment.locale('pt_br');
Vue.use(VueToastr, vueToastrConfig);
Vue.config.productionTip = false;
Vue.filter('roomMessageFormat', roomMessageFormatFilter);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
