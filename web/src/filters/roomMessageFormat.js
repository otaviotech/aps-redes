import moment from 'moment';

const RoomMessageFormat = (value) => {
  if (!value || !value.length || !moment(value).isValid()) {
    return '';
  }

  return moment(value).calendar();
};

export default RoomMessageFormat;
