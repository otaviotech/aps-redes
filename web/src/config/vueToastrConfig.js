const vueToastrConfig = {
  defaultPosition: 'toast-bottom-center',
  defaultType: 'info',
  defaultTimeout: 1000,
};

export default vueToastrConfig;
