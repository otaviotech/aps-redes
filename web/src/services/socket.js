import io from 'socket.io-client';
import { API_ADDRESS } from '@/config/env';

const socket = io.connect(API_ADDRESS);

const events = {
  CHAT: 'chat',
  CREATE_ROOM: 'create-room',
  ENTER_ROOM: 'enter-room',
  LEAVE_ROOM: 'leave-room',
  DELETE_ROOM: 'delete-room',
  ENTER_LOBBY: 'enter-lobby',
  LEAVE_LOBBY: 'leave-lobby',
  ROOM_UPDATED: 'room-updated',
  USER_ENTER_ROOM: 'user-enter-room',
  USER_LEAVE_ROOM: 'user-leave-room',
  DELETED_ROOM: 'deleted-room',
  NEW_MESSAGE_IN_ROOM: 'new-message-in-room',
};

const chat = (data) => {
  socket.emit(events.CHAT, {
    message: data.message,
  });
};

const createRoom = (data) => {
  socket.emit(events.CREATE_ROOM, {
    room: data.room,
    user: data.user,
  });
};

const enterRoom = (data) => {
  socket.emit(events.ENTER_ROOM, {
    user: data.user,
    room: data.room,
  });
};

const leaveRoom = (data) => {
  socket.emit(events.LEAVE_ROOM, {
    user: data.user,
    room: data.room,
  });
};

const enterLobby = (data) => {
  socket.emit(events.ENTER_LOBBY, {
    user: data.user,
  });
};

const leaveLobby = (data) => {
  socket.emit(events.LEAVE_LOBBY, {
    user: data.user,
  });
};

const deleteRoom = () => {
  socket.emit(events.DELETE_ROOM, {
    room: '',
  });
};

const emit = {
  chat,
  createRoom,
  enterRoom,
  leaveRoom,
  deleteRoom,
  enterLobby,
  leaveLobby,
};

export default {
  socket,
  events,
  emit,
};
