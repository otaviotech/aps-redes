/**
 * @function validate
 * @description Valida um objeto de sala.
 * @param {object} room A sala a ser validada.
 * @returns {object} Um objeto de validação com as propriedades:
 *  - {boolean} success Se o objeto passou na validação ou não.
 *  - {array} errors Os erros encontrados na validação.
 */
const validate = (room) => {
  const result = {
    success: true,
    errors: [],
  };

  if (!room.name || !room.name.length) {
    result.success = true;
    result.errors.push({
      prop: 'name',
      msg: 'Informe um nome correto.',
    });
  }

  if (!room.maxUsers || room.maxUsers < 1) {
    result.success = false;
    result.errors.push({
      prop: 'maxUsers',
      msg: 'A quantidade de usuários deve ser maior ou igual a 1.',
    });
  }

  return result;
};

const RoomValidationService = {
  validate,
};

export default RoomValidationService;
