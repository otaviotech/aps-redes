import axios from 'axios';
import { API_ADDRESS } from '@/config/env';

const RoomService = {
  deleteRoom(roomId) {
    return axios.delete(`${API_ADDRESS}/room/${roomId}`);
  },
};

export default RoomService;
