const storeUser = (user) => {
  window.localStorage.setItem('user', JSON.stringify(user));
};

const getUser = () => {
  const user = window.localStorage.getItem('user');
  return user ? JSON.parse(user) : null;
};

const logout = () => {
  window.localStorage.removeItem('user');
};

export default {
  getUser,
  storeUser,
  logout,
};
