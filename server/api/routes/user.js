const express = require('express');
const router = express.Router();
const UserRepository = require('../../infra/repositories/user');

router.get('/', (req, res, next) => {
    UserRepository.getAll().then((results) => {
        res.status(200).json({
            message: 'Users were fetched',
            data: results,
        });
    })
});

router.post('/', (req, res, next) => {
    UserRepository.create(req.body).then((newUser) => {
        res.status(201).json({
            message: 'Usuário cadastrado com sucesso! Seja bem vindo!',
            user: newUser,
        });
    });
});

router.get('/:userId', (req, res, next) => {
    UserRepository.get(req.params.userId).then((user) => {
        res.status(200).json({
            message: 'Success',
            user,
        });
    });
});

router.delete('/:userId', (req, res, next) => {
    UserRepository.destroy(req.params.userId).then((result) => {
        res.status(200).json({
            message: 'User deleted',
            userId: req.params.userId,
        });
    })
});

module.exports = router;