const express = require('express');
const router = express.Router();
const RoomRepository = require('../../infra/repositories/room');
const ChatSocket = require('../../chat/socket');
const ChatSocketEvents = require('../../chat/socketEvents');

router.get('/', (req, res, next) => {
    RoomRepository.getAll().then((results) => {
        res.status(200).json({
            message: 'Rooms were fetched',
            data: results,
        });
    })
});

router.patch('/addUser', (req, res, next) => {
    RoomRepository.addUser(req.body.roomId, req.body.userId)
        .then((results) => {
            res.status(200).json({
                message: 'Usuário adicionado a sala',
                data: results,
            });
        })
        .catch((err) => {
            res.status(500).json({
                message: err.message,
            });
        });
});

router.patch('/removeUser', (req, res, next) => {
    RoomRepository.removeUser(req.body.roomId, req.body.userId)
        .then((results) => {
            res.status(200).json({
                message: 'Usuário removido da sala',
                data: results,
            });
        })
        .catch((err) => {
            res.status(500).json({
                message: err.message,
            });
        });
});

router.post('/', (req, res, next) => {
    RoomRepository.create(req.body)
        .then((newRoom) => {
            ChatSocket.getConnection().sockets.emit(ChatSocketEvents.CREATE_ROOM, newRoom);
            res.status(201).json({
                message: 'Sala criada com sucesso!',
                room: newRoom,
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                message: 'Erro ao criar sala.',
                room: null,
            });
        });
});

router.get('/:roomId', (req, res, next) => {
    RoomRepository.get(req.params.roomId).then((room) => {
        res.status(200).json({
            message: 'Success',
            room,
        });
    });
});

router.delete('/:roomId', (req, res, next) => {
    RoomRepository.destroy(req.params.roomId).then((result) => {
        res.status(200).json({
            message: 'Room deleted',
            roomId: req.params.roomId,
        });
    })
});

module.exports = router;