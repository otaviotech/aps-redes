const mongoose = require('mongoose');
const db = require('../db');
const User = require('../db/models/user');

const getAll = () => {
    return User.find().exec();
};

const create = async (user) => {
    user._id = mongoose.Types.ObjectId();
    return await User.create(user);
};

const update = async (user) => {
    return await User.findByIdAndUpdate(user._id, {
        $set: {
            name: user.name,
            email: user.email,
            socketId: user.socketId,
        }
    }).exec();
};

const destroy = (id) => {
    return User.remove({ _id: user.id});
};

module.exports = {
    getAll,
    create,
    update,
    destroy,
};