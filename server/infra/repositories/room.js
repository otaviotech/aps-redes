const mongoose = require('mongoose');
const User = require('../db/models/user');
const Room = require('../db/models/room');
const Message = require('../db/models/message');
const moment = require('moment');

const getAll = () => {
    return Room.find().exec();
};

const get = (id) => {
    return Room.findById(id).exec();
};

const create = async (room) => {
    room._id = mongoose.Types.ObjectId();
    return await Room.create(room);
};

const update = (room) => {
    return Room.find({
        id: room._id
    }).update().exec();
};

const destroy = (id) => {
    return Room.remove({
        _id: id
    });
};

const addUser = async (roomId, userId) => {
    return Room.findById(roomId).exec().then((room) => {
        if (room.users && room.users.find((u) => u._id == userId)) {
            return Promise.reject({
                message: 'Usuário já está na sala.',
            });
        }
        User.findById(userId).exec().then((user) => {
            room.users.push(user);
            return room.save();
        });
    })
    .catch(err => console.log(err));
};

const addMessage = async (data) => {
    Room.findById(data.message.room._id).exec().then((room) => {
        data.message._id = mongoose.Types.ObjectId();
        data.message.roomId = room._id;
        // data.message.sender = data.message.user;
        data.message.timestamp = moment().toISOString();
        // console.log(room);
        Message.create(data.message).then((message) => {
            room.messages.push(message);
            return room.save();
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
};

const removeUser = async (roomId, userId) => {
    return Room.findById(roomId).exec().then((room) => {
        if (!room.users.find((a) => a._id == userId)) {
            return Promise.reject({
                message: 'Usuário não está na sala.'
            });
        }

        User.findById(userId).exec().then((user) => {
            room.users = room.users.filter(u => u ._id != userId);
            return room.save();
        });
    });
};


module.exports = {
    get,
    getAll,
    create,
    update,
    destroy,
    addUser,
    removeUser,
    addMessage,
};