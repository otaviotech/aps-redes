const mongoose = require('mongoose');
const user = require('./user');
const room = require('./room');

const messageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    timestamp: String,
    text: String,
    sender: user.schema,
    roomId: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('Message', messageSchema);