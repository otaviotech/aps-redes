const mongoose = require('mongoose');
const user = require('./user');
const message = require('./message');

const roomSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    maxUsers: Number,
    usersOnline: Number,
    owner: user.schema,
    users: [user.schema],
    messages: [message.schema],
});

module.exports = mongoose.model('Room', roomSchema);