const mongoose = require('mongoose');
const env = require('../../env');

let connection;

const setup = () => {
    const mongoDBconnectionString = `mongodb://${env.mongodb.username}:${env.mongodb.password}@${env.mongodb.host}:${env.mongodb.port}/${env.mongodb.database}?authSource=admin`;
    mongoose.connect(mongoDBconnectionString)
        .then((successMongoose) => {
            connection = successMongoose.connection;
            console.log('Conectado ao MongoDB.');
        })
        .catch((error) => console.log('Erro na conexão com o MongoDB.'));
};

const getConnection = () => connection;

module.exports = { 
    connection,
    getConnection,
    setup,
};