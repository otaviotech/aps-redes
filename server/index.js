const express = require('express');
const mongo = require('./infra/db');
const socketServer = require('./chat/socket');
const api = require('./api');
const env = require('./env');

// Express
const app = express();

// Configurando a API RESTFUL.
api.setup(app);

const server = app.listen(env.app.port, () => {
    console.log(`Aplicação aceitando requisições na porta ${env.app.port}.`);
});

// MongoDB
const db = mongo.setup();

// Servidor de socket (SocketIO)
const socket = socketServer.setup(server);