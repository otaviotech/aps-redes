const socketIO = require('socket.io');
const events = require('./socketEvents');
const UserRepository = require('../infra/repositories/user');
const RoomRepository = require('../infra/repositories/room');

let socketConn;

const setup = (server) => {
    socketConn = socketIO(server);
    // console.log(socketConn);

    socketConn.on('connection', (socket) => {
        socket.on('disconnect', (data) => { 
            console.log('Um usuário se desconectou'); 
        });
        
        socket.on(events.CHAT, (data) => {
            RoomRepository.addMessage(data).then((res) => {
                socketConn.sockets.emit(events.NEW_MESSAGE_IN_ROOM, data);
            })
            .catch(err => console.log(err));
        });

        socket.on(events.ENTER_LOBBY, (data) => {
            console.log(`Usuário ${data.user.name} entrou no lobby.`);
            data.user.socketId = socket.id;
            UserRepository.update(data.user)
                .then((res) => {
                    socketConn.sockets.emit(events.ENTER_LOBBY, data);
                })
                .catch((err) => {
                    
                });
        });

        socket.on(events.CREATE_ROOM, (data) => {
            socketConn.sockets.emit(events.CREATE_ROOM, data);
        });
        
        socket.on(events.ENTER_ROOM, (data) => {
            RoomRepository.addUser(data.room._id, data.user._id)
                .then((res) => {
                    console.log(`Usuário ${data.user.name} entrou na sala ${data.room.name}.`);
                    socketConn.sockets.emit(events.USER_ENTER_ROOM, data);
                })
                .catch((err) => {
                    console.log(err);
                });
        });

        socket.on(events.LEAVE_ROOM, (data) => {
            RoomRepository.removeUser(data.room._id, data.user._id)
                .then((res) => {
                    console.log(`Usuário ${data.user.name} saiu da sala ${data.room.name}.`);
                    socketConn.sockets.emit(events.USER_LEAVE_ROOM, data);
                })
                .catch((err) => {
                    console.log(err);
                });
        });

        socket.on(events.LEAVE_LOBBY, (data) => {
            socketConn.sockets.emit(events.LEAVE_LOBBY, data);
        });

        socket.on(events.DELETE_ROOM, (data) => {
            socketConn.sockets.emit(events.DELETED_ROOM, data);
        });
    })
    
};

const getConnection = () => socketConn;

module.exports = {
    getConnection,
    setup,
};
