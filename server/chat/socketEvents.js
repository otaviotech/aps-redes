module.exports = {
    CHAT: 'chat',
    CREATE_ROOM: 'create-room',
    ENTER_ROOM: 'enter-room',
    LEAVE_ROOM: 'leave-room',
    DELETE_ROOM: 'delete-room',
    ENTER_LOBBY: 'enter-lobby',
    LEAVE_LOBBY: 'leave-lobby',
    ROOM_UPDATED: 'room-updated',
    USER_ENTER_ROOM: 'user-enter-room',
    USER_LEAVE_ROOM: 'user-leave-room',
    DELETED_ROOM: 'deleted-room',
    NEW_MESSAGE_IN_ROOM: 'new-message-in-room',
};